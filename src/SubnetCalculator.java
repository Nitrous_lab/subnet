
import java.net.InetAddress;
import java.net.UnknownHostException;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author slmfr
 */
public class SubnetCalculator {
    private InetAddress ip;
    private InetAddress mask;
    private byte[] ipByte ;
    private byte[] maskBytes;
    private byte[] networkAddress;
    private byte[] broadcastAddress;
    private byte[] firstUsable;
    private byte[] lastUsable;
    



    public SubnetCalculator(String ipAddress, String subnetMask) throws UnknownHostException  {
        this.ip = InetAddress.getByName(ipAddress);
        this.mask = InetAddress.getByName(subnetMask);
        this.ipByte = this.ip.getAddress();
        this.maskBytes = this.mask.getAddress();
        this.broadcastAddress =  calculateBroadcast();
        this.networkAddress = calculateNetworkAddress();
        calculateRang();
        
        
        
    }
    public byte[] calculateNetworkAddress(){
        byte[] networkAddress = new byte[ipByte.length];
            for (int i = 0; i < this.ipByte.length; i++) {
                networkAddress[i] = (byte) (ipByte[i] & maskBytes[i]);
            }
            
            return  networkAddress;
    }
    
        public byte[] calculateBroadcast(){
            byte[] broadcastAddress = new byte[ipByte.length];
            for (int i = 0; i < this.ipByte.length; i++) {
                broadcastAddress[i] = (byte) (ipByte[i] | (255 - maskBytes[i]));
            }
            
            return broadcastAddress;
    }

    public InetAddress getIp() {
        return ip;
    }

    public InetAddress getMask() {
        return mask;
    }

    public byte[] getIpByte() {
        return ipByte;
    }

    public byte[] getMaskBytes() {
        return maskBytes;
    }

    public String getNetworkAddress() throws UnknownHostException {
        return "Network Address: " + InetAddress.getByAddress(this.networkAddress).getHostAddress();
    }

    public String getBroadcastAddress() throws UnknownHostException {
        return "Broadcast Address: " + InetAddress.getByAddress(this.broadcastAddress).getHostAddress();
    }

    public String getFirstUsable() throws UnknownHostException {
        return "First Usable IP: " + InetAddress.getByAddress(this.firstUsable).getHostAddress();
    }

    public String getLastUsable() throws UnknownHostException {
        return "Last Usable IP: " + InetAddress.getByAddress(this.lastUsable).getHostAddress();
    }
        
    public void calculateRang(){
            this.firstUsable = networkAddress.clone();
            this.lastUsable = broadcastAddress.clone();
            this.firstUsable[firstUsable.length - 1] += 1;
            this.lastUsable[lastUsable.length - 1] -= 1;

            
           
    }
    public int NetworkPrefixLength() {
       
        int prefixLength = 0;
        for (byte b : maskBytes) {
            int maskValue = b & 0xFF;
            if (maskValue == 255) {
                prefixLength += 8;
            } else if (maskValue == 0) {
                break;
            } else {
               
                while ((maskValue & 0x80) != 0) {
                    prefixLength++;
                    maskValue <<= 1;
                }
                break;
            }
        }

        return prefixLength;
    }
    
    
    public String getNumberOfUsableHosts() {
    int totalAddresses = (int) Math.pow(2, 32 - NetworkPrefixLength());
    int usableHosts = totalAddresses - 2;
    return "Number of Usable Host : "+usableHosts;
}
}
    
       